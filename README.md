jteach完整版本(源码, jar文件)下载地址: http://code.google.com/p/jteach <br />

<h3>一。关于Jteach</h3>
jteach是使用java开发的一个小巧，跨平台的教学软件。

主要功能：

1.屏幕广播

2.屏幕监视 + 控制 + 客户机广播

3.文件传输

4.远程命令执行(例如，关机命令)

...

<h3>二。运行环境要求：</h3>

安装了JDK的主机。并且客户机能够找到服务器机器（如果是局域网那就对了）

当然可以在没有安装JDK的电脑上运行，如果有需求请和我联系 chenxin619315@gmail.com。

使用方法和教程，请参考附件jteach-{version}-src-jar.zip里面的README文档 imges文件夹里面为界面图片效果。

1.运行服务器端（教师端）：java -jar tteach-server-{version}.jar

2.运行客户端（学生端）：输入服务器端的IP，点击connect即可。

1.jteach-1.2.6-src-jar.zip版, 处理软件断线处理外还提供一定的硬件掉线处理能力(例如网线断了).

2.jteach-1.2-src-jar.zip版, 没有硬件断线处理能力(具有软件断线处理能力,例如关闭软件),速度比1.2.6的快.

<h3>三. Jteach其他主页: </h3>
Jcseg官方主页: https://code.google.com/p/jteach/ <br />
Jcseg开源中国: http://www.oschina.net/p/jteach <br />

<h3>四. 联系作者: </h3>
作者: 狮子的魂 <br />
email: chenxin619315@gmail.com <br />
qq: 1187582057 <br />

